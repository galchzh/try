<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;

/**
 * ArticleSearch represents the model behind the search form of `app\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */

    public $tag;
    public $globalSearch;

    public function rules()
    {
        return [
            [['id', 'author_id', 'editor_id', 'category_id', 'created_by', 'updated_by'], 'integer'],
            [['title', 'description', 'body', 'created_at', 'updated_at','tag','globalSearch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'author_id' => $this->globalSearch,
            'editor_id' => $this->globalSearch,
            'category_id' => $this->globalSearch,
            'created_at' => $this->globalSearch,
            'updated_at' => $this->globalSearch,
            'created_by' => $this->globalSearch,
            'updated_by' => $this->globalSearch,
            
        ]);

        $query->andFilterWhere(['like', 'title', $this->globalSearch])
            ->andFilterWhere(['like', 'description', $this->globalSearch])
            ->andFilterWhere(['like', 'body', $this->globalSearch]);

        //Add tags condition
        
        if(!empty($this->tag))
        {
           $condition = Tag::find()
                    ->select('id')
                    ->where(['IN', 'name', $this->globalSearch]);
            $query->joinWith('tags');
            $query->andWhere(['IN', 'tag_id', $condition]);

        }

        return $dataProvider;
    }
}
